﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace TwoDStyle
{
    public abstract class TwoDStyleRenderer : TwoDStyleObject
    {
        public bool useSecurityStateList = false;
        public List<string> renderingStatePossible;
        public delegate void StateChangedHandler(string renderingState);
        public event StateChangedHandler OnStateChanged;

        private string renderingState;
        public string RenderingState
        {
            get { return (renderingState); }
            set {
                if (useSecurityStateList && renderingStatePossible != null)
                {
                    bool validString = false;
                    foreach (string state in renderingStatePossible)
                    {
                        if( value == state)
                        {
                            validString = true;
                            break;
                        }
                    }
                    if(!validString)
                    {
                        if(Debug.isDebugBuild)
                        {
                            Debug.LogError("Try to set a uncorrect string to " + gameObject.name + "'s renderingState. (no string named '" + value + "' appear in this TwoDStyleRenderer's List).");
                        }
                        return;
                    }
                }
                renderingState = value;
                if (OnStateChanged !=  null)
                {
                    OnStateChanged(renderingState);
                }
            }
        }


        //public enum ObjectState { Yolo, Swag };
        [SerializeField]
        protected Transform sideRender;
        void OnEnable()
        {
            CamerasCallToRenderer.onPreCull += FixTheCamera;
        }

        void OnDisable()
        {
            CamerasCallToRenderer.onPreCull -= FixTheCamera;
        }
        /// <summary>
        /// Make  the Gameobject actual plate render to face the transform given, usualy use by  Cameras before render.
        /// </summary>
        public abstract void FixTheCamera(Transform actualCam);
        /// <summary>
        /// the Side plate of any TwoDStyleRenderer
        /// </summary>
        public Transform SideRender
        {
            get
            {
                return sideRender;
            }
            set
            {
                sideRender = value;
            }
        }
        /*
        protected virtual void Start()
        {

        }*/
        /*protected virtual void Update()
        {

        }*/


    }
}
